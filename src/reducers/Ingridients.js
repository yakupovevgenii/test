const initialState = {
	hand: 0,
	cucumber: 0,
	leg: 0

}
export default function sumCoins(state = initialState, action) {
		
	switch (action.type){ 
		case 'SET_HAND':
			return {...state, hand: action.payload};
		case 'SET_CUCUMBER':
			return {...state, cucumber: action.payload}
		case 'SET_LEG':
			return {...state, leg: action.payload}
		default:
			return state;
	}
}
