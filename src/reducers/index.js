import {combineReducers} from 'redux'
import coins from './coins.js'
import ingredients from './Ingridients.js'

export default combineReducers({
  coins, ingredients
  
})