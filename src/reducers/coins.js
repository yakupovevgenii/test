const initialState = {
	coin: 19,
	ok: 'т'

}
export default function sumCoins(state = initialState, action) {
		
	switch (action.type){ 
		case 'SET_COIN':
			return {...state, coin: action.payload};
		case 'SET_OK':
			return {...state, ok: action.payload}
		default:
			return state;
	}
}
