import React, { Component } from 'react'
import '../sass/style.sass'
import { bindActionCreators } from 'redux'
import Text from '../components/text.js'
import Line from '../components/line.js'
import Coins from '../components/coins.js'
import Button from '../components/button.js'
import AddHuman from '../components/AddHuman.js'
import ImgIngredient from '../components/ImgIngredient.js'
import {connect} from 'react-redux'  
import * as coinActions from '../actions/CoinActions'
import * as IngredientsActions from '../actions/IngredienrsActions.js'
import hand from '../img/hand.png'
import leg from '../img/leg.png'
import cucumber from '../img/cucumber.png'

class App extends Component {
  render() {
	
		const {setCoins, setOk} = this.props.coinActions
		const {setHand, setCucumber, setLeg} = this.props.IngredientsActions	
    return <div className="app">
			<Text className="title" classNameP="titleP" Value="Фабрика человечков" />
			<Line Value="Копилка" />
			<Coins setCoins={setCoins} setOk={setOk} />
			<Line Value="Рынок ингредиентов" />
			<div className="buyIngredientGroup">
				<Button id="price5" ingredientStoreValue={this.props.ingredients.hand} setSell={setHand} setCoins={setCoins} classNameBtn="Ingredient buyIngredient" name="Купить ручку" price={5} priceP="за 5 монет"/>
				<Button id="price7" ingredientStoreValue={this.props.ingredients.leg} setSell={setLeg} setCoins={setCoins} classNameBtn="Ingredient buyIngredient" name="Купить ножку" price={7} priceP="за 7 монет"/>
				<Button id="price15" ingredientStoreValue={this.props.ingredients.cucumber} setSell={setCucumber} setCoins={setCoins} classNameBtn="Ingredient buyIngredient" name="Купить огуречек" price={20} priceP="за 20 монет"/>
			</div>
			<Line Value="Ингредиенты в мешке" />
			<div className="buyIngredientGroup">
				<div>
					<ImgIngredient className="INmgIngredient" price={3} src={hand} Value={this.props.ingredients.hand}/>
					<Button id="price5" ingredientStoreValue={this.props.ingredients.hand} setSell={setHand} sell setCoins={setCoins}  classNameBtn="Ingredient sellIngredient" name="Продать одну" price={3} priceP="за 3 монет"/>
				</div>
				<div>
					<ImgIngredient className="INmgIngredient" price={5} src={leg} Value={this.props.ingredients.leg}/>
					<Button id="price7" ingredientStoreValue={this.props.ingredients.leg} setSell={setLeg} sell setCoins={setCoins} classNameBtn="Ingredient sellIngredient" name="Продать одну" price={5} priceP="за 5 монет"/>
				</div>
				<div>
					<ImgIngredient className="INmgIngredient" price={15} src={cucumber} Value={this.props.ingredients.cucumber}/>
					<Button id="price15" ingredientStoreValue={this.props.ingredients.cucumber} sell setSell={setCucumber} setCoins={setCoins} classNameBtn="Ingredient sellIngredient" name="Продать один" price={15} priceP="за 15 монет"/>
				</div>
			</div>
			<Line Value="Производство человечка" />
			<div>
				<AddHuman 
				setSellH={setHand}
				setSellL={setLeg}
				setSellC={setCucumber}
				handStoreValue={this.props.ingredients.hand}
				legStoreValue={this.props.ingredients.leg}
				cucumberStoreValue={this.props.ingredients.cucumber}/>

			</div>



		</div>
  }
}
function mapStateToProps (state) {
  return {
    coin: state.coins,
    ingredients: state.ingredients    
  }
}
function mapDispatchToPrors(dispatch){
	return{
		coinActions: bindActionCreators(coinActions, dispatch),
		IngredientsActions: bindActionCreators(IngredientsActions, dispatch)
	}
}
export default connect(mapStateToProps, mapDispatchToPrors)(App)
