import React, { Component } from 'react'
import {connect} from 'react-redux'
class ImgIngredient extends Component {

componentWillReceiveProps(nextProps) {

			if (nextProps.Value == 0 || (this.props.price+nextProps.coin.coin)>100){			
				this.setState({
					textColor: true
				})
			}
			else{
				this.setState({
					textColor: false
			})
			}	
}	
componentWillMount(){
	if (this.props.Value == 0 || (this.props.price+this.props.coin.coin)>100){			
				this.setState({
					textColor: true
				})
			}
			else{
				this.setState({
					textColor: false
			})
			}	
}	

  render() {

    return (
			<div style={this.state.textColor ? {color: 'red'} : {color: 'white'}} className="containerImg" >
				<div>
					<img className="INmgIngredient" src={this.props.src}></img>
				</div>
				<div className="PImgX">
					<p>X</p>
				</div >
				<div className="AmountIngredintsImgP">
					<p> {this.props.Value}</p>
				</div>
			</div>	
			)
  }
}
function mapStateToProps(state) {
	return {
		coin: state.coins,
		ingredients: state.ingredients
	}
}
export default connect(mapStateToProps)(ImgIngredient)