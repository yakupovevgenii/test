import React, { Component } from 'react'
import {connect} from 'react-redux'
class Button extends Component{
	
constructor(props) {
		super(props)
	
			this.state = {
				withOpacity: false,
				textColor: false

			}
}
btnOnclickSale(){
		let coinPlus = this.props.coin.coin
		let price = this.props.price
		if (this.props.sell) {
			
			if (this.props.ingredientStoreValue>=1 && (price+coinPlus)<=100) {
		
				this.props.setSell(this.props.ingredientStoreValue - 1)
				this.props.setCoins(coinPlus + this.props.price)
			}		
		}
		else if (!this.props.sell && coinPlus>=price) {

			this.props.setCoins(coinPlus - this.props.price)
			this.props.setSell(this.props.ingredientStoreValue + 1)
			var width = (((coinPlus-price)*5)+19)
			document.getElementById('img-coins').style.width = `${width}px`			
		}
	}
componentWillReceiveProps(nextProps) {

		if (!this.props.sell) {
			if (nextProps.coin.coin <	this.props.price ){			
				this.setState({
					withOpacity: true
				})
			}
			else{
				this.setState({
					withOpacity: false
			})
			}
		}	
		else if (this.props.sell) {
			
			if (nextProps.ingredientStoreValue == 0 || (this.props.price+nextProps.coin.coin)>100){			
				this.setState({
					withOpacity: true
				})
			}
			else{
				this.setState({
					withOpacity: false
			})
			}	
		}
}	
componentWillMount(){
	if (!this.props.sell) {
		if (this.props.coin.coin <	this.props.price ){			
			this.setState({
				withOpacity: true,
				textColor: true
			})
		}
		else{
			this.setState({
				withOpacity: false,
				textColor: false
		})
		}
	}
	else if (this.props.sell) {

		if (this.props.ingredientStoreValue == 0 || (this.props.price+this.props.coin.coin)>100 ){			
			this.setState({
				withOpacity: true,
				textColor: true
			})
		}
		else{
			this.setState({
				withOpacity: false,
				textColor: false
		})
		}
	}
}	
	render () {
		return(
			<div>
				<div>
					<button style={this.state.withOpacity ? {opacity: 0.4} : {opacity: 1}} sell={this.props.sell} className={this.props.classNameBtn} onClick={::this.btnOnclickSale}>{this.props.name}</button>
				</div>
				<div>
					<p  style={this.state.withOpacity ? {opacity: 0.4} : {opacity: 1}} className={`${this.props.className}P`} >{this.props.priceP}</p>			
				</div>
			</div>
			)
	}
} 
function mapStateToProps(state) {
	return {
		coin: state.coins,
		ingredients: state.ingredients
	}
}

export default connect(mapStateToProps)(Button)