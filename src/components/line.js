import React, { Component } from 'react'

export default class Line extends Component {
	constructor(props) {
			super(props);
			this.props=props;
	}
  render() {
    return <div className="line">
			<div className="hrLine">
				<hr className="hrLineLeft"></hr>
			</div>
			<div>
				<p className="titleBlock">{this.props.Value}</p>
			</div>
			<div className="hrLine">
				<hr className="hrLineRight"></hr>
			</div>
		</div>
  }
}