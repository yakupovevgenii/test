import React, { Component } from 'react'

export default class Text extends Component {
	constructor(props) {
			super(props);
			this.props=props;
	}
  render() {
    return <div className="text">
			<div className={this.props.className}>
				<p className={this.props.classNameP}>{this.props.Value}</p>
			</div>
		</div>
  }
}