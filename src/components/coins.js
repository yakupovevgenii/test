import React, {PropTypes, Component } from 'react'
import coi from '../img/coin.png';
import {connect} from 'react-redux' 


class Coins extends Component {
	coin() {
		let arr = [];
		for (let i = 0; i < this.props.coin.coin & i<100; i++) {
			arr.push(coi);
		}
		return arr;
	}
	splitToDigits(number) {
		var digits = [];
		while (number) {
			digits.push(number % 10);
			number = Math.floor(number/10);
		}
		return digits;
	}	
	onCoinBtnClick(){
		let coinPlus = parseInt(this.props.coin.coin)
		if (coinPlus<100) {
			this.props.setCoins(coinPlus+1)			
		}
	}
componentWillReceiveProps(nextProps){
	
	if (nextProps.coin.coin > this.props.coin.coin || nextProps.coin.coin < this.props.coin.coin  ) {
		let width = (((nextProps.coin.coin+1)*5)+19)
		let a = this.splitToDigits(nextProps.coin.coin)
		if (a[0]==0||a[0]>4&a[0]<10){this.props.setOk('т')}
		else	if (a[0]==1){this.props.setOk('та')}
		else if (a[0]>1&a[0]<5) {this.props.setOk('ты')}	
		document.getElementById('img-coins').style.width = `${width}px`
	}
}
  render() {
		return(
				<div className="container">
					<div className="coins-container">
							<div id="img-coins" className="img-coins">
							{this.coin().map((img, index) => (<img key={index}  
								style={{ left:`${-19*index}px`, zIndex:`${-2*index}`} } className="im" src={img} />))}
							</div>
					</div>
					<div className="pCoins">
						<p className="coins">{this.props.coin.coin} моне{this.props.coin.ok}</p>
					</div>
					<div className="onCoinBtnClick-container">
								<button className="onCoinBtnClick" onClick={::this.onCoinBtnClick}> Добавить одну монету </button>
					</div>				
				</div> );
  }
}
function mapStateToProps (state) {
  return {
    coin: state.coins,
    ok: state.coins
  }
}
Coins.propTypes = {
	setOk: PropTypes.func.isRequired,
	setCoins: PropTypes.func.isRequired
}
export default connect(mapStateToProps)(Coins)