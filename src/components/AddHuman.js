import React, { Component } from 'react'
import ButtonIngredient from '../components/ButtonIngredient.js'
import handAdd from '../img/handAdd.png'
import handNone from '../img/handNone.png'
import hand from '../img/hand.png'
import legAdd from '../img/legAdd.png'
import legNone from '../img/legNone.png'
import leg from '../img/leg.png'
import cucumberAdd from '../img/cucumberAdd.png'
import cucumberNone from '../img/cucumberNone.png'
import cucumber from '../img/cucumber.png'
export default class Line extends Component {
	constructor(props) {
			super(props);
			this.props=props;
			this.state = {
				srcc: true
			}
	}


  render() {
    return (
		<div>
			<div className="container-radio-group">
				<div className="radio-group">
					<p>Пол:</p>	
				</div>
				<div className="radio-group">
					<input type="radio" id="option-one" name="selector"/><label htmlFor="option-one">Мужчина</label>
					<input type="radio" id="option-two" name="selector"/><label htmlFor="option-two">Женщина</label>
				</div>
			</div>

			<div className="container-radio-group">
				<div className="radio-group">
						<p>Цвет:</p>	
				</div>
				<div className="radio-group">
					<input type="radio" id="option-oneq" name="selector1"/><label htmlFor="option-oneq">Белый</label>
					<input type="radio" id="option-twoq" name="selector1"/><label htmlFor="option-twoq">Черный</label>
				</div>
			</div>
			<div className="btnGroupIngredient">

				<ButtonIngredient
					handNone={handNone}
					StoreValue={this.props.handStoreValue}
					srcAddHand={handAdd}
					src={hand}
					setSell={this.props.setSellH}
					/>
					<ButtonIngredient
					handNone={handNone}
					StoreValue={this.props.handStoreValue}
					srcAddHand={handAdd} 
					src={hand}
					setSell={this.props.setSellH}
					/>
					<ButtonIngredient
					handNone={legNone}
					StoreValue={this.props.legStoreValue}
					srcAddHand={legAdd}
					src={leg}
					setSell={this.props.setSellL}
					/>
					<ButtonIngredient
					handNone={legNone}
					StoreValue={this.props.legStoreValue}
					srcAddHand={legAdd}
					src={leg}
					setSell={this.props.setSellL}
					/>
					<ButtonIngredient
					handNone={cucumberNone}
					StoreValue={this.props.cucumberStoreValue}
					srcAddHand={cucumberAdd} 
					src={cucumber}
					setSell={this.props.setSellC}
					/>
			</div>
			<div>
				
			</div>
		</div>
		)
  }
}