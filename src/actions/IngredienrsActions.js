export function setHand(hand){
	return{
		type: 'SET_HAND',
		payload: hand
	}	
}
export function setCucumber(cucumber){
	return{
		type: 'SET_CUCUMBER',
		payload: cucumber
	}
}
export function setLeg(leg){
	return{
		type: 'SET_LEG',
		payload: leg
	}
}
